# -*- coding: utf-8 -*-

import logging; log = logging.getLogger(__name__)
from pyramid.view import view_config, forbidden_view_config
from pyramid.response import Response
from . import lib
import os
import mimetypes



@view_config(route_name='login', renderer='templates/login.pt')
def login_view(request): # TODO debug code to be re-written or dropped ASAP
    next = request.params.get('next') or request.route_url('home')
    login = ''
    did_fail = False
    if 'submit' in request.POST:
        from pyramid.security import remember
        login = request.POST.get('login', '')
        passwd = request.POST.get('passwd', '')
        if 'pitchum' in login:
            headers = remember(request, login)
            from pyramid.httpexceptions import HTTPFound
            return HTTPFound(location=next, headers=headers)
        did_fail = True
    
    return {
        'login': login,
        'next': next,
        'failed_attempt': did_fail,
    }

@forbidden_view_config()
def forbidden(request):
    return Response('forbidden')

@view_config(route_name='home', renderer='templates/home.pt', permission='authenticated')
def home_view(request):
    return {
        'albums': lib.albums(),
    }

@view_config(route_name='repo.home', renderer='templates/repo_home.pt')
def repo_home(request):
    reponame = request.matchdict.get('reponame')
    log.debug('repo_home visited')
    log.debug('reponame=%s' % reponame)
    return {
        'reponame': reponame,
    }

@view_config(route_name='album.home.withslash', renderer='templates/album_home.pt')
@view_config(route_name='album.home', renderer='templates/album_home.pt')
def album_home(request):
    album_name = request.matchdict.get('name')
    items = lib.album_items(album_name)
    
    return {
        'album_name': album_name,
        'items': items,
    }

@view_config(route_name='album.download')
def album_download(request):
    album_name = request.matchdict.get('album_name')
    data = ""
    zip_filename = lib.zip_album(album_name)
    mt, encoding = mimetypes.guess_type(zip_filename)
    with open(zip_filename, 'rb') as f:
        data = f.read()
        zip_basename = os.path.basename(zip_filename)
        return Response(body=data,content_type=mt,content_disposition='attachment; filename=' + zip_basename)
    return Response("")

@view_config(route_name='item.preview')
def item_preview(request):
    album_name = request.matchdict.get('album_name')
    item_name = request.matchdict.get('item_name')
    data = ""
    thumb_filename = lib.get_thumb_filename(album_name, item_name)
    mt, encoding = mimetypes.guess_type(thumb_filename)
    with open(thumb_filename, 'rb') as f:
        data = f.read()
        return Response(body=data,content_type=mt)
    return Response("")

@view_config(route_name='item')
def item(request):
    album_name = request.matchdict.get('album_name')
    item_name = request.matchdict.get('item_name')
    data = ""
    filename = lib.get_display_filename(album_name, item_name)
    mt, encoding = mimetypes.guess_type(filename)
    with open(filename, 'rb') as f:
        data = f.read()
        return Response(body=data,content_type=mt)
    return Response("")

@view_config(route_name='item.viewer', renderer='templates/item.pt')
def item_viewer(request):
    album_name = request.matchdict.get('album_name')
    item_name = request.matchdict.get('item_name')
    item = lib.item(album_name, item_name)
    items = [e for e in lib.album_items(album_name)]
    i = items.index(item)
    prev_item = items[i - 1] if i > 0 else None
    next_item = items[i + 1] if i < len(items) - 1 else None
    return {
        'album_name': album_name,
        'item_name': item_name,
        'item': item,
        'prev_item': prev_item,
        'next_item': next_item,
    }
