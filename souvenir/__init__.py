# -*- coding: utf-8 -*-

from pyramid.config import Configurator
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
import pkg_resources
from . import lib

__version__ = pkg_resources.require("souvenir")[0].version

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    settings['app_version'] = __version__
    config = Configurator(settings=settings)
    config.include('pyramid_chameleon')
    config.set_root_factory(RootFactory)
    
    lib.init_dirs(config)
    
    # Default chameleon template to be inherited
    # See http://docs.pylonsproject.org/projects/pyramid_cookbook/en/latest/templates/templates.html#using-a-beforerender-event-to-expose-chameleon-base-template
    config.add_subscriber('souvenir.subscribers.add_base_template', 'pyramid.events.BeforeRender')
    
    # Configure auth mechanisms
    authn_policy = AuthTktAuthenticationPolicy('seekrit', hashalg='sha512', callback=groupfinder)
    config.set_authentication_policy(authn_policy)
    authz_policy = ACLAuthorizationPolicy()
    config.set_authorization_policy(authz_policy)
    
    # Declare views
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('login', '/signin') # TODO debug code to be re-written or dropped ASAP
    config.add_route('repo.home', '/repo/{reponame}')
    config.add_route('album.home.withslash', '/album/{name}/')
    config.add_route('album.home', '/album/{name}')
    config.add_route('album.download', '/album/{album_name}/download')
    config.add_route('item.preview', '/thumb/{album_name}/{item_name}')
    config.add_route('item.viewer', '/album/{album_name}/{item_name}')
    config.add_route('item', '/item/{album_name}/{item_name}')
    config.scan('souvenir')
    return config.make_wsgi_app()

class RootFactory(object): # TODO debug code to be re-written or dropped ASAP
    from pyramid.security import Allow, Authenticated, ALL_PERMISSIONS
    __acl__ = [
        (Allow, Authenticated, ALL_PERMISSIONS),
        (Allow, 'admin', ALL_PERMISSIONS),
    ]
    
    def __init__(self, request):
        self.request = request

def groupfinder(userid, request): # TODO debug code to be re-written or dropped ASAP
    if 'pitchum' in userid:
        return ['admin']
    return []
