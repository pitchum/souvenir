# -*- coding: utf-8 -*-

import logging ; log = logging.getLogger(__name__)
import sys, os
from zipfile import ZipFile
import pyramid
import subprocess
from PIL import Image, ImageFile
from PIL.ExifTags import TAGS

ImageFile.MAXBLOCK = 1000000 # cf. https://mail.python.org/pipermail/image-sig/1999-August/000816.html

cache_dir = ""
repos_home = ""
app_basedir = os.path.abspath(os.getcwd())

def init_dirs(config):
	'''Creates missing folders required for the webapp.'''
	cache_dir = config.registry.settings.get('souvenir.cache.dir')
	if not os.path.exists(cache_dir):
		os.makedirs(cache_dir)
		log.debug("Folder created: %s" % cache_dir)

	repos_home = config.registry.settings.get('souvenir.repo.parent.dir')
	if not os.path.exists(repos_home):
		os.makedirs(repos_home)
		log.debug("Folder created: %s" % repos_home)

def cache_dir(album_name):
	return os.path.join(pyramid.threadlocal.get_current_registry().settings.get('souvenir.cache.dir'), album_name)

def thumb_dir(album_name):
	return os.path.join(pyramid.threadlocal.get_current_registry().settings.get('souvenir.cache.dir'), album_name, 'thumbs')

def album_dir(album_name):
	repos_home = pyramid.threadlocal.get_current_registry().settings.get('souvenir.repo.parent.dir')
	return os.path.join(repos_home, album_name)

def albums():
	albums_parentdir = pyramid.threadlocal.get_current_registry().settings.get('souvenir.repo.parent.dir')
	return sorted([album_name for album_name in os.listdir(albums_parentdir)])

def item(album_name, item_filename):
	abs_filename = os.path.join(album_dir(album_name), item_filename)
	item_name, ext = os.path.splitext(item_filename)
	item_type = 'picture'
	if ext.lower() in ('.mts', '.avi', '.mp4'): # XXX hard-coded filetype detection
		item_type = 'video'
	return {
		'name': item_name,
		'filename': item_filename,
		'type': item_type,
	}

def album_items(album_name):
	def test(dirname):
		for filename in sorted(os.listdir(dirname)):
			item_name, ext = os.path.splitext(filename)
			item_type = 'picture'
			if ext.lower() in ('.mts', '.avi', '.mp4'): # XXX hard-coded filetype detection
				item_type = 'video'
			yield {
				'name': item_name,
				'filename' : filename,
				'type': item_type,
			}
	return test(album_dir(album_name))

def get_thumb_filename(album_name, item_name, thumb_maxsize=(300,300)):
	orig_filename = os.path.join(album_dir(album_name), item_name)
	thumbdir = thumb_dir(album_name)
	if not os.path.exists(thumbdir):
		os.makedirs(thumbdir, exist_ok=True)
	basename, ext = os.path.splitext(item_name)
	thumb_filename = os.path.join(thumbdir, basename + '.jpg')
	if os.path.isfile(thumb_filename):
		return thumb_filename
	
	if orig_filename.lower().endswith('.mts') or orig_filename.lower().endswith('.avi') or orig_filename.lower().endswith('.mp4'):
		create_thumbnail_for_video(orig_filename, thumb_filename, thumb_maxsize)
	else:
		create_thumbnail_for_picture(orig_filename, thumb_filename, thumb_maxsize)
	log.debug("Created thumb %s" % thumb_filename)
	return thumb_filename

def get_display_filename(album_name, item_name, item_maxsize=(300,300)):
	orig_filename = os.path.join(album_dir(album_name), item_name)
	outdir = cache_dir(album_name)
	if not os.path.exists(outdir):
		os.makedirs(outdir)
	basename, ext = os.path.splitext(item_name)
	
	if orig_filename.lower().endswith('.mts') or orig_filename.lower().endswith('.avi') or orig_filename.lower().endswith('.mp4'): # XXX hard-coded filetype detection
		optimized_filename = os.path.join(outdir, basename + '.webm')
		if not os.path.isfile(optimized_filename):
			transcode_video(orig_filename, optimized_filename, item_maxsize)
	else:
		optimized_filename = os.path.join(outdir, basename + '.jpg')
		if os.path.isfile(optimized_filename):
			return optimized_filename
		# Taken from http://andrius.miasnikovas.lt/2010/04/creating-thumbnails-from-photos-with-python-pil/
		img = Image.open(orig_filename)
		exif = img._getexif()
		if exif != None:
			for tag, value in exif.items():
				decoded = TAGS.get(tag, tag)
				if decoded == 'Orientation':
					if value == 3: img = img.rotate(180)
					if value == 6: img = img.rotate(270)
					if value == 8: img = img.rotate(90)
					break
		img.resize(item_maxsize, Image.ANTIALIAS)
		img.save(optimized_filename, 'JPEG', quality=100, progressive=True, optimize=True)
		log.debug("created %s" % optimized_filename)
	return optimized_filename

def create_thumbnail_for_picture(orig_filename, thumb_filename, thumb_maxsize=(500,500)):
	# Taken from http://andrius.miasnikovas.lt/2010/04/creating-thumbnails-from-photos-with-python-pil/
	img = Image.open(orig_filename)
	exif = img._getexif()
	if exif != None:
		for tag, value in exif.items():
			decoded = TAGS.get(tag, tag)
			if decoded == 'Orientation':
				if value == 3: img = img.rotate(180)
				if value == 6: img = img.rotate(270)
				if value == 8: img = img.rotate(90)
				break
	img.thumbnail(thumb_maxsize, Image.ANTIALIAS)
	img.save(thumb_filename, 'JPEG', quality=100, progressive=True, optimize=True)

def create_thumbnail_for_video(orig_videofilename, thumb_filename, thumb_maxsize=(320,240)):
	border_filename = os.path.join(app_basedir, 'souvenir/static/img/movie_tapes/border240.png')
	intermediate_filename = thumb_filename + ".tmp.jpg"
	dimensions = '%dx%d' % (thumb_maxsize[0], thumb_maxsize[1])
	intermediate_dimensions = '320x240'  # TODO compute best dimensions for thumbnail depending on video's aspect ratio
	cmd1 = ['ffmpeg', '-y', '-itsoffset', '3', '-i', orig_videofilename, '-vframes', '1', '-s', intermediate_dimensions, intermediate_filename ]
	log.debug(" ".join(cmd1))
	subprocess.check_call(cmd1, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
	cmd2 = ['convert', '-size', intermediate_dimensions, 'xc:skyblue', intermediate_filename, '-geometry', '+0+0', '-composite',
		border_filename, '-geometry', '+0+0', '-composite',
		border_filename, '-geometry', '+300+0', '-composite',
		thumb_filename]
	log.debug(" ".join(cmd2))
	subprocess.check_call(cmd2, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

def transcode_video(orig_videofilename, transcoded_videofilename, max_dimensions=None):
	'''
	Transcodes a video to the WebM format.
	'''
	if not os.path.isfile(orig_videofilename):
		raise Exception('File does not exist: %s' % orig_videofilename)
	
	cmd = """
ffmpeg -y -i %(orig_videofilename)s
 -threads 1
 -c:v libvpx -r 25 -b:v 500k -qmin 10 -qmax 42 -minrate 500k -maxrate 500k -bufsize 20k -vf scale=640:-1
 -c:a libvorbis
 -sn
 -f webm %(transcoded_videofilename)s
""" % {'orig_videofilename': orig_videofilename, 'transcoded_videofilename': transcoded_videofilename}
	log.debug(" ".join(cmd.split()))
	log.info('Started transcoding video "%s". This may take a while...' % transcoded_videofilename)
	subprocess.check_call(cmd.split(), stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
	log.info("Created video %s" % transcoded_videofilename)

def zip_album(album_name):
	zipfilename = os.path.join(cache_dir(album_name), album_name + ".zip")
	if os.path.exists(zipfilename):
		# TODO we should re-create the ZIP if its content differs from the album content
		return zipfilename
	album_path = album_dir(album_name)
	with ZipFile(zipfilename, 'w') as outfile:
		for filename in [item.get("filename") for item in album_items(album_name)]:
			absfilename = os.path.join(album_path, filename)
			arcname = album_name + "/" + filename
			outfile.write(absfilename, arcname=arcname)
	log.debug("created %s" % zipfilename)
	return zipfilename
