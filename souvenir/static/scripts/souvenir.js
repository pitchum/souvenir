/*
 * Default config values for jQuery plugins
 */
// $.blockUI.defaults.message = '<img src="static/img/loading.gif"/>';
// $.blockUI.defaults.overlayCSS = { 
	// padding: '5px',
	// margin: '-5px',
	// borderRadius: '3px',
	// backgroundColor: '#444',
	// opacity: 0.6,
// };
// $.blockUI.defaults.css = { 
	// borderWidth: '0px', 
	// backgroundColor: 'transparent', 
// };

/*
 * Generic handling of Ajax responses.
 */
$(document.body).on('ajaxError', function (evt, jqXhr, ajaxSettings, thrownError) {
	$.growlUI('AjaxError ' + ajaxSettings.url, thrownError);
});
$(document.body).on('ajaxComplete', function (event, xhr, ajaxOptions) {
	if (xhr.status != 200) {
		var title = "Code " + xhr.status;
		var body = "url: " + ajaxOptions.url + "\n\n" + xhr.responseText;
		console.error(title, '\n', body);
		$.growlUI(title, body.replace(/\n/g, '<br/>'));
		return true;
	}
	var data = null;
	try {
		data = JSON.parse(xhr.responseText);
	} catch (e) {
		// C'est pas dit que les requêtes Ajax retourneront toujours du JSON.
		return true;
	}
	if (!data) {
		return true;
	}
	if (data.debug) {
		console.debug(data.debug);
	}
	if (data.info) {
		console.info(data.info);
		$.growlUI("Information", data.info);
	}
	if (data.warn) {
		console.warn(data.warn);
		$.growlUI("Avertissement", data.warn);
	}
	if (data.error) {
		console.error(data.error);
		$.growlUI("Error", data.error);
	}
	if (data.exception) {
		console.error(data.exception);
		$.growlUI("Exception", data.exception);
	}
});


var default_options = {
	zoomMin: 0.9,
	zoomMax: 1.3,
	rotMin: -15,
	rotMax: 15,
};
function shuffle(img, options) {
	var opt = {};
	opt = $.extend(opt, default_options);
	opt = $.extend(opt, options);

	var rot = Math.round(Math.random() * (opt.rotMax - opt.rotMin) + opt.rotMin);
	var scale = Math.random() * (opt.zoomMax - opt.zoomMin) + opt.zoomMin;
	scale = Math.round(scale * 10) / 10;
	// console.debug('rotate(' + rot + 'deg) scale(' + scale + ');');
	img.setAttribute('style', '-moz-transform: rotate(' + rot + 'deg) scale(' + scale + '); -webkit-transform: rotate(' + rot + 'deg) scale(' + scale + ');')
}

function shuffle_all() {
	var imgs = document.querySelectorAll('#items img');
	for (var i = 0; i < imgs.length; i++) {
		var img = imgs[i];
		shuffle(img);
	}
}

$(document.body).on('mouseenter', '#items img', function() {
	shuffle(this, {zoomMin: 1.5, zoomMax: 2});
	$(this).css('z-index', '5000');
});
$(document.body).on('mouseout', '#items img', function() {
	shuffle(this);
	$(this).css('z-index', 'auto');
});

$(document).on("pageinit", function() {
	var maxHeight = $(window).height() + "px";
	$("section.item video, section.item img").css("max-height", maxHeight);
	shuffle_all();
});
