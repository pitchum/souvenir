souvenir
========

souvenir is a web photo album written in python that doesn't need an SQL database.

## Requirements

- `ffmpeg`: to create thumbnails and transcode videos
- [pyramid](https://docs.pylonsproject.org/projects/pyramid/en/latest/ ): a web framework written in python
- [PIL](https://python-pillow.org/ ): Python Imaging Library


## Developer quickstart guide

Instructions for Debian, you may need to adapt.

    sudo apt-get install python3-pyramid python3-pil
    git clone https://pitchum@bitbucket.org/pitchum/souvenir.git
    cd souvenir
    python3 setup.py develop --user
    pserve3 developement.ini --reload

 Now simply create subfolders in `/var/tmp/souvenir_data/albums` with
 photos/videos in them.
